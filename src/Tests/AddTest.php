<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Src\Entity\Service;
use Src\Manager\ServiceManager;

final class newTest extends TestCase
{
    public function addService(): void
    {
        //  1 -   Instanciation de mon ServiceManager
        $serviceManager = new ServiceManager();
        //  2 -   Création du Service manuelle pour tester le ADD 
        $newService = new Service(0, "Nouveau service", 100);
        // 0 pour l'id comme la BDD s'en charge, mais obligatoire dans le constructeur
        $serviceManager->add($newService);
        // 3 -    Récuperer le dernier ID inserer en BDD
        $lastId = (int)$serviceManager->getConnection()->lastInsertId();
        // 4 -    Select sur le dernier id afin de vérifier mon insert 
        $serviceInsert = $serviceManager->findById($lastId);
        $newService->setId($lastId);
        $this->assertSame($newService, $serviceInsert);
    }
}
