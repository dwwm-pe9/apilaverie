<?php

namespace Src\Controller;


use Src\Entity\Service;
use Src\Manager\ServiceManager;


class ServiceController
{   
    private ServiceManager $serviceManager;

    
    public function __construct()
    {
        //J'injecte un nouvelle objet ServiceManager dans mon constructeur 
        //Pour l'utiliser dans mon controller avec $this->serviceManager->findById() par exemple
        $this->serviceManager = new ServiceManager();
    }

    /*
    ** Route GET: /findById/$id 
    ** @param $id ID d'un Service
    */
    public function findById(int $id)
    {   
        //TODO Retouver le service en BDD
        $service = $this->serviceManager->findById($id);

        if ($service) {
            echo json_encode($service);
        } else {
            echo json_encode("Service not found!");
        }
    }

    /*
    ** Route GET:  /service  
    */
    public function findAll()
    {
        //TODO Retouver tout les services en BDD
        echo json_encode($this->serviceManager->findAll());
    }

    /*
    ** Route POST /add
    */
    public function add()
    {   
        //TODO Vérifier la methode POST 
       
        //TODO Récuperer et verifier les parametres POST recu
        
            //TODO Construire un Service avec les parametres

            // Sauvegarde en BDD

            echo json_encode("Service added successfully!");

    }

    /*
    ** Route POST /edit/$id
    ** @param $id ID d'un Service
    */
    public function edit(int $id)
    {   
        //TODO Verifier la methode POST 

        //TODO Verifier si le service avec cette id existe et le recuperer dans $service

        $service = true;

        if ($service) {

            //TODO Verifier les parametres post

                //TODO  Modifier les valeurs du Service

                //Le sauvegarder en BDD

                echo json_encode("Service updated successfully!");

        } else {
            echo json_encode("Service not found!");
        }
    }

    /*
    ** Route DELETE : /delete/$id
    ** @param $id ID d'un Service
    */
    public function delete(int $id)
    {
        //TODO Verifier la methode DELETE

        //TODO Verifier si le service avec cette id existe et le recuperer dans $service
        
        $service = true;

        if ($service) {

            //TODO Supprimer le Service en BDD

            echo json_encode("Service deleted successfully!");

        } else {
            echo json_encode("Service not found!");
        }
    }


}
