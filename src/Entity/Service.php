<?php

namespace Src\Entity;

use JsonSerializable;

/**
 * Service represente une occurence de la table Service sous forme d'objet PHP
 */
class Service implements JsonSerializable
{

    private int $id;
    private string $name;
    private float $price;


    public function __construct(int $id, string $name, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /*
    ** Methode permettant de transformer un Service en chaine de caractere, encodable en JSON
    */
    public function jsonSerialize(): array
    {

        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "price" => $this->getPrice()
        ];
    }

   /*
    ** Methode permettant de transformer un tableau Service
    */
    static public function fromArray(array $array): self
    {
        return new self($array["id"], $array["name"], $array["price"]);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */
    public function setPrice(float $price)
    {
        $this->price = $price;

        return $this;
    }
}
