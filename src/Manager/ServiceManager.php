<?php

namespace Src\Manager;

use Exception;
use PDOException;
use Src\Entity\Service;

/**
 * ServiceManager representera un outil permettant de faire les requetes relatives a l'Entity Service
 */
class ServiceManager extends DatabaseManager
{
    public function findAll()
    {
        $query = $this->getConnection()->prepare("SELECT * FROM service");
        $query->execute([]);

        $results = $query->fetchAll();
        $services = [];

        foreach ($results as $result) {
            $services[] = Service::fromArray($result);
        }

        return $services;
    }

    public function findById(int $id): Service|false
    {
        $query = $this->getConnection()->prepare("SELECT * FROM service WHERE id = :id");
        $query->execute([":id" => $id]);

        //Verifier si j'ai un resultat
        $res = $query->fetch();


        if ($res === false) {
            return $res;
        }

        //Convertir le resultat de la requete en Objet
        return  Service::fromArray($res);
    }

    public function add(Service $service)
    {
        try {
            $response = $this->getConnection()->prepare("INSERT INTO service (name, price) VALUES (:name, :price)");
            //Dupliquer la solution pour edit 

            $response->execute(
                [
                    ':name' => $service->getName(),
                    ':price' => $service->getPrice(),
                ]
            );
        } catch (Exception $e) {
            echo ("Erreur lors de l'ajout en BDD");
            exit();
        }
    }

    public function edit(Service $service)
    {
        try {
            $response = $this->getConnection()
                ->prepare("UPDATE service SET name = :name, price = :price WHERE id = :id");

            $response->execute(
                [
                    ':id' => $service->getId(),
                    ':name' => $service->getName(),
                    ':price' => $service->getPrice(),
                ]
            );
        } catch (Exception $e) {
            echo($e->getMessage());
            exit();
        }
    }

    public function delete(int $id)
    {

        $query = $this->getConnection()->prepare("DELETE FROM service WHERE id = :id");

        $query->execute([
            ':id' => $id
        ]);
    }
}
