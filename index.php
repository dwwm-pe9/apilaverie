<?php

//Utilisation de l'autoload pour require tous les fichiers dans src/

use Src\Controller\ServiceController;
use Src\Manager\ServiceManager;

require_once('vendor/autoload.php');

// --------------------------------------------------------
// --------------------------------------------------------
// TODO suivre les étapes entre chaque TODO et les réaliser
// Ne pas modifier le code de cette page sauf si demandé 

//Lancer la commande
// composer install
// composer dump-autoload


// ----------- 
//  TP Utilisation d'un router et d'un controller


// L'objectif est d'utiliser des URLS de ce type
// http://localhost/apilaverie/index.php/service/add
// http://localhost/apilaverie/index.php/service/12

//Au lieu de par exemple
//  http://localhost/apilaverie/index.php?action=add&class=service
//  http://localhost/apilaverie/index.php?action=find&class=service&id=12


//Pour cela il va falloir découper l'url 
//et récuperer la partie intéressante 

//Récuperation de l'url 
$uri = $_SERVER['REQUEST_URI'];

// Extrait la partie de l'URI après '/index.php/' pour recuperer seulement la suite
//Par exemple
// /service/add
// /service/12

//Recuperer la partie de la chaine $uri apres /index.php
$basePath = '/index.php/';
//Trouver position de index.php
$indexPos = strpos($uri, $basePath);
$indexLen = strlen($basePath);
//Recuperer la partie de $uri apres index.php/
$route = substr($uri,  $indexPos + $indexLen);
//-----------------position + longueur->                                   
// //http://localhost/apilaverie/index.php/service/12
// ---------------------------++++++++++                       
// //$route = /service/12

// Séparer les segments de la route dans un tableau a chaque / 
// exemple index.php/service/edit/12
$segments = explode('/', $route);
//// $segments = ["service","12"]


//Si il y une route principale je la recupere sinon je prend null
$mainRoute = !empty($segments[0]) ? $segments[0] : null;
// Route principale (ex: /service/, /user/.... )

//Si il y une route secondaire je la recupere sinon je prend null
$subRoute =  !empty($segments[1]) ? $segments[1] : null;
// Route pour les action ( service add ,edit ,delete ou un id, null pour le findAll)


// DEBUT TODO
// dump("Mon url segmenté", $segments);
// dump("\$mainRoute = $mainRoute");
// dump("\$subRoute = $subRoute");
//Comme index.php est un fichier on reste dedans meme en mettant des / apres dans l'url 

//  Modifier l'url pour tester la valeur des dump 
//  Comprendre l'utilisation de $mainRoute et $subRoute

// /index.php/service
// index.php/service/add 
// index.php/service/edit/12

// Remarquer que si on ne mets pas de mainroute on a un bug et la subroute vaut index.php ( ne rien modifier )
// FIN TODO

//--------------------------
// CHECK au besoin
//--------------------------

//Commenter ce die
//die();

// Afin d'utiliser efficacement l'orienté objet nous allons nous reposer sur le modèle MVC
// index.php sera maintenant notre unique point d'entrer et devra avec l'url
// Appeler le bon code et faire les bonnes actions

// On appele cela un ROUTER


//Il sera désormais créer des condition selon les url, parametres,  id....

//Afin de gerer les differentes routes demandées avec l'URL 
//et fournir les bonnes actions

//Nous allons gérer les routes suivantes

//      /service                Tous les services
//      /service/"id"           Service par "id"
//      /service/add
//      /service/edit/"id"      Edit par "id"
//      /service/delete/"id"    Delete par "id"


// Toutes les autres routes renverrons un message "page not found"


//La méthode de la requete sera également utile
// dump("Method : " . $_SERVER['REQUEST_METHOD']);


header("Content-type: application/json");
// ----------
//  CODE DU ROUTER

//Ce Switch va agir comme une liste de if avec le parametre $mainRoute
switch ($mainRoute) {
        //TODO Reduire le case pour voir la fin de ce switch
        //Pour chaque case on test la mainRoute $mainRoute === "service"
    case 'service':
        //On passe ici dans les routes concernant les Services
        //j'appele ici mon Controller associé
        $serviceController = new ServiceController(); 

        // Vérifier si un ID est fourni apres la subRoute
        $hasNumericID = !empty($segments[2]) && is_numeric($segments[2]);
        // Indice dans $segments     0      1   2
        // -----------------------service/edit/12
        
        //On teste maintenant le sous parametre ( add edit delete ) avec un autre switch
        switch ($subRoute) {

            case 'add':

                echo "ROUTE: /service/add   (add)";

                break;

            case 'edit':

                // Vérifier si un ID est fourni 3ieme param
                if ($hasNumericID) {

                    echo "ROUTE: /service/edit/$segments[2]   (edit)";
                } else {
                    echo "ROUTE: /service/edit/ Bad Request: Missing ID";
                }

                break;

            case 'delete':

                // Vérifier si un ID est fourni
                if ($hasNumericID) {

                    echo "ROUTE: /service/delete/$segments[2]  (delete)";
                } else {
                    echo "ROUTE: /service/delete/  Request: Missing ID";
                }

                break;

            case null:
                // service/
                // Route pour récupérer tous les services
                $serviceController->findAll();

                break;
                // S'il y a un segment mais qu'il ne correspond à aucune sous-route
            default:
                // Vérifier si le segment correspond à un ID numérique
                if (is_numeric($subRoute)) {
                    // Route pour récupérer le service correspondant l'ID 
                    $serviceController->findById($subRoute);
                    
                } else {
                    echo "Page not found";
                    //Toutes autres routes
                    // service/****
                }
                break;
        }
        break;
    default:
        // Si $mainRoute ne correspond à aucune route connue
        //Dans notre exemple il n'y a que /service, 
        echo "Page not found";
        break;
}

//TODO Essayer d'aficher le résultat de chaque case et acceder a chaque routes


//Le router utilisera le Controller correspondant à la mainRoute
// et la fonction du controller correspondant à la subRoute


// DEBUT TODO Connecter vos routes aux fonction du controller ServiceController, 

// Commencer par la route  FIND BY ID

// Se referer au fichier tests.php pour utliser les objets et retrouver le code source

// Completer le code de index en ne modifiant que les cases

// 1 Importer ServiceController l'instancier puis utiliser ses méthodes dans les cases 
// 2 Remplacer les echo "ROUTE: /" des case FIND BY ID par la méthode du controller correspondante

// 3 Faire les TODO de  ServiceController pour la fonction FIND BY ID 

// 4 Aller sur la route findById et tenter de retrouver un élément de la BDD

// FIN TODO

//--------------------------
// CHECK
//--------------------------

// TODO continuer avec FINDALL et DELETE

// Debut TODO Completer les ADD EDIT


// Afin de tester toutes vos routes méthodes GET, POST, DELETE ...
// Utiliser un outil comme Postman ou Bruno
// Permet de modifier rapidement les URL sauvegarder les requetes etc

// Config Bruno fournie ApiCollection (racine du projet à importer dans Bruno)

// Il ne reste qu'a tester les routes de la colleciton en remplissant le param nécessaire pour ADD / EDIT

// FIN TODO

//--------------------------
// CHECK
//--------------------------


// TODO renvoyer des erreurs au format JSON en cas de parametres manquant ( POST, mauvais format etc...)
