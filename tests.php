<?php

use Src\Entity\Service;
use Src\Manager\ServiceManager;

//Utilisation de l'autoload pour require tous les fichiers dans src/
require_once('vendor/autoload.php');

//Content type représente le type de données contenu dans le body
//header("Content-type: application/json");
//Nous sommes dans une API donc tout ce que nous renverrons sera au format JSON

// ------------------------- DEBUT TP ------------------------

// Ne pas modifier ce fichier seulement ServiceManager


// TODO Lancer le code de tests de vos fonctions ADD, EDIT, DELETE

//  1 -   Instanciation de mon ServiceManager
$serviceManager = new ServiceManager();
//  2 -   Création du Service manuelle pour tester le ADD 
$newService = new Service(0, "Nouveau service", 100);
// 0 pour l'id comme la BDD s'en charge, mais obligatoire dans le constructeur
$serviceManager->add($newService);
// 3 -    Récuperer le dernier ID inserer en BDD
$lastId = (int)$serviceManager->getConnection()->lastInsertId();
// 4 -    Select sur le dernier id afin de vérifier mon insert 
$serviceInsert = $serviceManager->findById($lastId);
dump("NEW SERVICE", $serviceInsert);

// 5 -    Modification de serviceInsert pour tester le EDIT
$serviceInsert->setName("Nouveau service Modifié");
$serviceInsert->setPrice(200);
$serviceManager->edit($serviceInsert);

// 6 -    Select sur l'id de serviceEdit afin de vérifier l'update en BDD 
$serviceEdit = $serviceManager->findById($serviceInsert->getId());
dump("EDITED SERVICE", $serviceEdit);

// 7 -    Test de la fonction DELETE
$serviceManager->delete($serviceEdit->getId());

// 8 -    Rappel de l'id pour verifier le DELETE en BDD .
$serviceDeleted = $serviceManager->findById($lastId);

dump("DELETED SERVICE", $serviceDeleted);
//Si le serviceDeleted = false, c'est qu'il n'y a pas de résultat, tout est OK, niveau du ADD UPDATE DELETE
// TODO Corriger l'erreur de lorsqu'il n'y a pas de résultat pour la fonction findById

// TODO Changer la ligne de la fonction add
//  ':name' => $service->getName(),
//  ':price' => $service->getPrice(),
// par
//  ':name' => "[]",
//  ':price' => "m",

//Relancer les tests

// Trouver une solution pour n'afficher que le message d'erreur en chaine de caractere et exit() le code.
//Dupliquer la solution pour edit 


// ------------------------- FIN TP ------------------------

//Affichage du resultat en encodant les données en JSON
//echo (json_encode($res));
//Lorsque j'utilise json_encode sur un Objet, il ne renverra que les proprietes PUBLIC, si ma Class implemente JSONSerializable
// La fonction jsonSerialize de ma classe sera appellée
